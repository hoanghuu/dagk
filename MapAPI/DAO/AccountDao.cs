﻿using MapAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MapAPI.DAO
{
    public class AccountDao: AbstractDao
    {
        public AccountDao():base()
        {

        }
        public IEnumerable<Account> GetAccounts()
        {
            using ( con = GetConnection())
            {

                open();///hehfdf

                string sql = "SELECT * FROM Account";
                var cmd = new SqlCommand(sql, con);

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr == null)
                    {
                        throw new NullReferenceException("No People Available.");
                    }
                    while (rdr.Read())
                    {
                        var person = new Account();
                        person.adress = rdr["name"].ToString();
                        person.birthday = Convert.ToDateTime(rdr["birthday"]);
                        person.latitude = Convert.ToDouble(rdr["latitude"]);
                        person.longitude = Convert.ToDouble(rdr["longitude"]);
                        person.name = rdr["name"].ToString();
                        person.state = rdr["state"].ToString();
                        person.username = rdr["username"].ToString();


                        yield return person;/// moi usda
                    }
                }
                close();
            }

                    }
        public Account getAcount(String username,String pass) 
        {
            using ( con = GetConnection())
            {
                open();
                string sql = String.Format("SELECT * FROM Account acc where acc.username='{0}' and acc.password='{1}'", username, pass);
                var cmd = new SqlCommand(sql, con);

                Account person=null;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr == null)
                    {
                        throw new NullReferenceException("No People Available.");
                    }
                    
                    while (rdr.Read())
                    {
                         person= new Account();
                         person.adress = rdr["adress"].ToString();
                        person.birthday = Convert.ToDateTime(rdr["birthday"]);
                        person.latitude = Convert.ToDouble(rdr["latitude"]);
                        person.longitude = Convert.ToDouble(rdr["longitude"]);
                        person.name = rdr["name"].ToString();
                        person.state = rdr["state"].ToString();
                        person.username = rdr["username"].ToString();

                        break;
                        
                        
                    }
                }
                close();
                return person ;    
            }
            
        }
        public Account getAcount(String username)
        {
            using ( con = GetConnection())
            {
                open();
                string sql = String.Format("SELECT * FROM Account acc where acc.username='{0}'", username);
                var cmd = new SqlCommand(sql, con);

                Account person = null;
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr == null)
                    {
                        throw new NullReferenceException("No People Available.");
                    }

                    while (rdr.Read())
                    {
                        person = new Account();
                        person.adress = rdr["adress"].ToString();
                        person.birthday = Convert.ToDateTime(rdr["birthday"]);
                        person.latitude = Convert.ToDouble(rdr["latitude"]);
                        person.longitude = Convert.ToDouble(rdr["longitude"]);
                        person.name = rdr["name"].ToString();
                        person.state = rdr["state"].ToString();
                        person.username = rdr["username"].ToString();

                        break;


                    }
                }
                close();
                return person;
            }

        }
        public int add(Account person)
        {
            int count=0;
            try
            {

                using (con = GetConnection())
                {
                    open();
                    SqlCommand com = new SqlCommand(String.Format("insert into Acccount values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7},'{8}')",
                        person.username, person.password, person.name, person.birthday, person.adress, person.latitude, person.longitude, person.state), con);
                    count = com.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw new Exception("insert failed");
            }
            finally
            {
                close();
            }
            return count;
        }
        public int update(Account person)
        {
            int count = 0;
            try
            {

                using (con = GetConnection())
                {
                    
                    open();
                    String sql = String.Format(@"update Account set name=N'{0}',birthday='{1:MM/dd/yyyy}',adress=N'{2} ',latitude='{3}',longitude='{4}',Account.state='{5}'
where  Account.username='{6}' and Account.password='{7}'",
                         person.name, person.birthday, person.adress, person.latitude, person.longitude, person.state,person.username,person.password);
                    SqlCommand com = new SqlCommand(sql, con);
                    count = com.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw new Exception("insert failed");
            }
            finally
            {
                close();
            }
            return count;
        }




        public int updateLocation(String username,MapAPI.DAO.Location location)
        {
            int count = 0;
            try
            {

                using (con = GetConnection())
                {
                    open();
                    String sql=String.Format("update Account set latitude='{0}',longitude='{1}',state='{2}' where  Account.username='{3}'",
                          location.Lat,location.Long, true, username);
                    SqlCommand com = new SqlCommand(sql, con);
                    count = com.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw new Exception("update failed");
            }
            finally
            {
                close();
            }
            return count;
        }

    }
}