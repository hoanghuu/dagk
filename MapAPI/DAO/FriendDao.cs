﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MapAPI.Models;
using System.Data.SqlClient;

namespace MapAPI.DAO
{
    public class FriendDao:AbstractDao
    {
        public FriendDao():base()
        {

        }

        public IEnumerable<Account> ViewListMyFiend(String UserName) {
            using (con = GetConnection())
            {
                open();
                string sql = @"select * from Account acc
                                where exists(select * from Friend f 
			                                where (f.acount = '"+UserName+@"' and acc.username=f.acountfriend ) 
			                                OR  (f.acountfriend = '"+UserName+@"' and acc.username=f.acount)
			                                )";
                var cmd = new SqlCommand(sql, con);

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr == null)
                    {
                        throw new NullReferenceException("No People Available.");
                    }
                    while (rdr.Read())
                    {
                        var person = new Account();
                        person.adress = rdr["name"].ToString();
                        person.birthday = Convert.ToDateTime(rdr["birthday"]); 
                        person.latitude = Convert.ToDouble(rdr["latitude"]);
                        person.longitude = Convert.ToDouble(rdr["longitude"]);
                        person.name = rdr["name"].ToString();
                        person.state = rdr["state"].ToString();
                        person.username = rdr["username"].ToString();

                        // cai yield nay la t ko bk roi asynresult  thay vi may
                        //ma gio m bị lỗi gì. ko vao dc ham nay.
                        // vay chi co debug ròi mò ra thôi
                       // m hỏi mấy đứa học môn này đi, t ko bk gì, mò lâu lắm.u7 cam on may
                        // detao team thang nhut
                        yield return person;// cai nay tao lay theo kieu Asyn requet tung lan 1=> tra ve 1 LIst<acount>, Hoi    NAY DC, GIO THI LOI
                    }
                }
                close();
            }

        }
        public IEnumerator<Account> findpersonbyHint(String name) 
        {
            name = name.ToLower();
            using ( con = GetConnection())
            {
                open();
                string sql = @"SELECT * FROM Account acc 
                              where  acc.username like '%"+name+"'";
                var cmd = new SqlCommand(sql, con);

                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr == null)
                    {
                        throw new NullReferenceException("No People Available.");
                    }
                    while (rdr.Read())
                    {
                        var person = new Account();
                        person.adress = rdr["name"].ToString();
                        person.birthday = Convert.ToDateTime(rdr["birthday"]);
                        person.latitude = Convert.ToDouble(rdr["latitude"]);
                        person.longitude = Convert.ToDouble(rdr["longitude"]);
                        person.name = rdr["name"].ToString();
                        person.state = rdr["state"].ToString();
                        person.username = rdr["username"].ToString();


                        yield return person;
                    }
                }
                close();
            }
        }
        public int add(Friend person)
        {
            int count = 0;
            con = GetConnection();
            open();
            SqlTransaction TRAN = con.BeginTransaction(System.Data.IsolationLevel.Serializable);
            
            try
            {

                
                
                    
                    SqlCommand com = new SqlCommand(String.Format("insert into Friend values('{0}','{1}')",
                        person.acount, person.acountfriend), con);
                    count = com.ExecuteNonQuery();
                    TRAN.Commit();

                
            }
            catch (Exception)
            {
                TRAN.Rollback();
               
            }
            finally
            {
                close();
            }
            return count;
        }
        public int delete(Friend person)
        {
            int count = 0;
            try
            {

                using (con = GetConnection())
                {
                    open();
                    SqlCommand com = new SqlCommand(String.Format(@"delete from Friend where (Friend.acount='{0}' and Friend.acountfriend='{1}') 
or (Friend.acount='{1}' and Friend.acountfriend='{0}')", person.acount, person.acountfriend), con);
                    count = com.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw new Exception("insert failed");
            }
            finally
            {
                close();
            }
            return count;
        }
        public Boolean ExistsFriend(Friend friendRelation) {
            bool isNotNull=false;
            try
            {

                using (con = GetConnection())
                {
                    open();
                    String sql = String.Format(@"select * from Friend f where (f.acount='{0}' and f.acountfriend='{1}') or (f.acount='{1}' and f.acountfriend='{0}')", friendRelation.acount, friendRelation.acountfriend);
                    SqlCommand com = new SqlCommand(sql, con);
                   isNotNull= com.ExecuteScalar()!=null;

                }
            }
            catch (Exception)
            {

                throw new Exception("not exists");
            }
            finally
            {
                close();
            }
            return isNotNull;   
        }
    }
}