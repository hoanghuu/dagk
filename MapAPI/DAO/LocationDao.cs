﻿using MapAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace MapAPI.DAO

{
    public class DistanceAlgorithm
    {
        const double PIx = 3.141592653589793;
        const double RADIO = 6371;//tính theo Km
        
        /// <summary>
        /// This class cannot be instantiated.
        /// </summary>
        private DistanceAlgorithm() { }

        /// <summary>
        /// Convert degrees to Radians
        /// </summary>
        /// <param name="x">Degrees</param>
        /// <returns>The equivalent in radians</returns>
        public static double Radians(double x)
        {
            return x * PIx / 180;
        }

        /// <summary>
        /// Calculate the distance between two places.
        /// </summary>
        /// <param name="lon1"></param>
        /// <param name="lat1"></param>
        /// <param name="lon2"></param>
        /// <param name="lat2"></param>
        /// <returns></returns>
        public static double DistanceBetweenPlaces(
            double lon1,
            double lat1,
            double lon2,
            double lat2)
        {
            double dlon = Radians(lon2 - lon1);
            double dlat = Radians(lat2 - lat1);

            double a = (Math.Sin(dlat / 2) * Math.Sin(dlat / 2)) + Math.Cos(Radians(lat1)) * Math.Cos(Radians(lat2)) * (Math.Sin(dlon / 2) * Math.Sin(dlon / 2));
            double angle = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            return (angle * RADIO) * 0.62137;//distance in km
        }

    } 
    public class LocationDao
    {
        public LocationDao()
        {

        }
        public static double GetDistanceInMeter(double curlatitude = 10.769736, double curlongitude = 106.672427, double deslatitude = 10.789919, double deslongitude = 106.690055)
        {
            string url = @"maps/api/distancematrix/json?origins="+curlatitude+","+curlongitude+"&destinations="+deslatitude+","+deslongitude+"&sensor=false";

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://maps.googleapis.com/");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(url).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                
                try
                {
                    var products = response.Content.ReadAsAsync<Models.DistanceMaTrix>().Result;
                    return Convert.ToDouble(products.rows[0].elements[0].distance.value);
                }
                catch (Exception)
                {

                    return 0;
                }

              
               
            }
            return 0;
        }
        public Location local;

        public LocationDao(Double Lat,Double Long)
        {
            local = new Location();
            local.Lat = Lat;
            local.Long = Long;
        }
        public LocationDao(String username)
        {
           local= findLocationMyFriendbyName(username);
        }
        public Location findLocationMyFriendbyName(String username) {
            return new Location() { Lat=1,Long=1};
        }
        public List<Account> findPersonsbyDistance(Account accountCenter,double dis) {
            FriendDao f=new FriendDao();
            IEnumerable<Account> lstfriend =f.ViewListMyFiend(accountCenter.username);

            List<Account> nearByFriends = new List<Account>();
            foreach (var item in lstfriend)
            {
                double distance = GetDistanceInMeter(accountCenter.latitude, accountCenter.longitude, item.latitude, item.longitude);
                if (distance<=dis && distance!=0)
                {
                    nearByFriends.Add(item);
                }
            }
            return nearByFriends;
            
        }
    }
}