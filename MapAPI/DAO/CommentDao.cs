﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MapAPI.Models;
using System.Data.SqlClient;

namespace MapAPI.DAO
{
    public class CommentDao:AbstractDao
    {
        public CommentDao():base()
        {

        }
        public int Add(Comment comment)
        {
            int count = 0;
            try
            {

                using (con = GetConnection())
                {
                    open();
                    SqlCommand com = new SqlCommand(String.Format("insert into Comment values({4},'{0}','{1}','{2}',N'{3}')",
                       comment.acount,comment.acountcomment,comment.commentdate,comment.content,-1), con);
                    count = com.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw new Exception("insert failed");
            }
            finally
            {
                close();
            }
            return count;
        }
        public int update(Comment comment)
        {
            int count = 0;
            try
            {

                using (con = GetConnection())
                {
                    open();
                    SqlCommand com = new SqlCommand(String.Format("update Comment set commentdate='{0}',content=N'{1}' where id='{2}')",
                        comment.commentdate, comment.content,comment.id), con);
                    count = com.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw new Exception("insert failed");
            }
            finally
            {
                close();
            }
            return count;
        }
        public IEnumerable<Comment> getWallComment(String usernameOfWall)
        {
            
            using (con = GetConnection())
            {
                open();
                string sql = String.Format(@"SELECT *
                            FROM Comment cm
                            where cm.acount='{0}'", usernameOfWall);
                var cmd = new SqlCommand(sql, con);
                
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr == null)
                    {
                        throw new NullReferenceException("No People Available.");
                    }
                    while (rdr.Read())
                    {
                        Comment comment;
                         comment= new Comment();
                        comment.id = Convert.ToInt32(rdr["id"]);
                        comment.acount = rdr["birthday"].ToString();
                        comment.acountcomment = Convert.ToString(rdr["latitude"]);
                        comment.commentdate = Convert.ToDateTime(rdr["longitude"]);
                        comment.content = rdr["name"].ToString();


                        yield return comment;
                    }
                }

            }
            
        }
        public Comment getComment(int id) 
        {
            Comment comment = null;
            using (var con = GetConnection())
            {
                open();
                string sql = String.Format(@"SELECT *
                            FROM Comment cm
                            where cm.id='{0}'", id);
                var cmd = new SqlCommand(sql, con);
                
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    if (rdr == null)
                    {
                        throw new NullReferenceException("No People Available.");
                    }
                    
                    while (rdr.Read())
                    {
                        
                        comment = new Comment();
                        comment.id = Convert.ToInt32(rdr["id"]);
                        comment.acount = rdr["birthday"].ToString();
                        comment.acountcomment = Convert.ToString(rdr["latitude"]);
                        comment.commentdate = Convert.ToDateTime(rdr["longitude"]);
                        comment.content = rdr["name"].ToString();


                        
                    }
                    
                }

            }
            return comment;
        }
        public bool RemoveAComment(int idComment,String username) 
        {

            int count = 0;
            try
            {

                using (con = GetConnection())
                {
                    open();
                    SqlCommand com = new SqlCommand(String.Format("delete Comment where id='{0}' and(acount='{1}' or acountcomment='{1}'",
                      idComment,username), con);
                    count = com.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw new Exception("insert failed");
            }
            finally
            {
                close();
            }
            return count>0;
        }
             
    }
}