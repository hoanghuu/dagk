﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace MapAPI.DAO
{
    public struct Location
    {

        
        public Double Long{ get; set; }
        public Double Lat { get; set; }
    }
    
    public abstract class AbstractDao
    {
        public static String _conStr;
        public static SqlConnection con;

        public static SqlConnection GetConnection() {
            try
            {
                return new SqlConnection(_conStr);
            }
            catch (Exception)
            {
                
                throw new Exception("please check your DataBase connection Again");
            }
            
        }
        public static void close() {
            if (con.State!=ConnectionState.Closed)
            {
                con.Close();
            }
        }
        public static void open() {
            if (con.State!=ConnectionState.Open)
            {
                con.Open();
            }
        }
        public AbstractDao()
        {
            _conStr = ConfigurationManager.ConnectionStrings["CSDL_MapService"].ConnectionString;
        }

    }
}