﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Thinktecture.IdentityModel.Http.Cors.Mvc;
using Thinktecture.IdentityModel.Http.Cors.WebApi;
namespace MapAPI.App_Start
{
   public class CorsConfig
    {
       public static void RegisterCors(HttpConfiguration httpConfig)
       {
           WebApiCorsConfiguration corsConfig = new WebApiCorsConfiguration();

           // this adds the CorsMessageHandler to the HttpConfiguration’s
           // MessageHandlers collection
           corsConfig.RegisterGlobal(httpConfig);

           // this allow all CORS requests to the Products controller
           // from the http://foo.com origin.
           corsConfig
               .ForResources("Values")
               .ForOrigins("http://localhost:2846")
               .AllowAll();

           corsConfig
               .ForResources("Account")
               .ForOrigins("http://localhost:2846")
               .AllowAll();
       }
       //private void RegisterCors(MvcCorsConfiguration corsConfig)
       //{
       //    corsConfig
       //    .ForResources("HocSinh")
       //    .ForOrigins("")
       //    .AllowAll();
       //}
    }
}