﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MapAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            
            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
               name: "Account",
               routeTemplate: "api/{controller}/{action}/{username}/{password}",
               defaults: new { username = "", password = RouteParameter.Optional }
           );
            
        }
    }
}
