﻿using MapAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace MapAPI.Controllers
 
{
       public class HttpResponseMessage<T> : HttpResponseMessage
{
    public HttpResponseMessage(T value, HttpStatusCode statusCode, System.Net.Http.Formatting.MediaTypeFormatter formatter)
    {
        StatusCode = statusCode;
        Content = new ObjectContent<T>(value, formatter);
    }
}

public static class HttpRequestExtensions
{
    public static HttpResponseMessage<T> CreateResponse<T>(this HttpRequestMessage request, T value, HttpStatusCode statusCode = HttpStatusCode.OK)
    {
        // NOTE: You'll need access to the configuration.
        var negotiator = GlobalConfiguration.Configuration.Services.GetContentNegotiator();
        var result = negotiator.Negotiate(typeof (T), request, GlobalConfiguration.Configuration.Formatters);
        return new HttpResponseMessage<T>(value, statusCode, result.Formatter);
    }
}
public class FriendController : ApiController
{
    
    //find friend by distance
    public IEnumerable<Account> Put(double id,[FromBody] Models.Account acc)
    {
        
        DAO.LocationDao dao = new DAO.LocationDao();
        List<Account> listFriend = dao.findPersonsbyDistance(acc, id);

            return listFriend;
        
        


    }

    //view list myfiriend
   [HttpGet]
    
    public  IEnumerable<Account> ListFriend(String id) 
    {
        DAO.FriendDao f = new DAO.FriendDao();

       return f.ViewListMyFiend(id);
        
    }
    [HttpDelete]
    public HttpResponseMessage remove([FromBody]Friend usename) //name of friend 
    {
        DAO.FriendDao f = new DAO.FriendDao();
        bool exists= f.ExistsFriend(usename);
        if (exists)
        {
            if (f.delete(usename) > 0)
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }
        throw new HttpResponseException(HttpStatusCode.NotFound);
    }
    [HttpPost]
    public HttpResponseMessage PostProduct([FromBody]Friend username)
    {
        DAO.FriendDao f = new DAO.FriendDao();
        bool added= f.add(username)>0;
        if (!added)
        {
            throw new HttpResponseException(HttpStatusCode.Conflict);
        }
        var response = Request.CreateResponse<Friend>(HttpStatusCode.Created, username);

        string uri = Url.Link("DefaultApi", new { id =RouteParameter.Optional});
        response.Headers.Location = new Uri(uri);
        return response;
    }
}

}
