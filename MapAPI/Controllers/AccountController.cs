﻿using MapAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MapAPI.Controllers
{
    public class AccountController : ApiController
    {
   

        // GET api/values/5
        public string Get(int id )
        {
            return "value";
        }
        //Đăng nhập
        [HttpGet]
        public Account login(string username,string  password) {
            MapAPI.DAO.AccountDao acDAO = new DAO.AccountDao();
           return acDAO.getAcount(username, password);
            
        }
        [HttpPost]
        public bool logout(string username, string password)
        {
            return false;
        }
        ///register
        [HttpPost]
        public HttpResponseMessage register([FromBody]Account ac) 
        {
            MapAPI.DAO.AccountDao acDAO = new DAO.AccountDao();
            Account varyfyAcc = acDAO.getAcount(ac.username);

            if (ac.username != null&& varyfyAcc !=null)
            {
                 throw new HttpResponseException(HttpStatusCode.Conflict);
            }
              var response = Request.CreateResponse<Account>(HttpStatusCode.Created, ac);

            string uri = Url.Link("DefaultApi", new { id = RouteParameter.Optional });
            response.Headers.Location = new Uri(uri);
            return response;
            
        }

    
        //change your account
        public void Put([FromBody]Account account)
        {
            MapAPI.DAO.AccountDao acDAO = new DAO.AccountDao();
            acDAO.update(account);
        }
        [HttpPost]
        public HttpResponseMessage UpdateLocation(String id, [FromBody]DAO.Location location) 
        {
            DAO.AccountDao acc = new DAO.AccountDao();
            int i= acc.updateLocation(id, location);
            
            if (i>0)
            {

                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotModified);
            }
 
        }
    }
}
