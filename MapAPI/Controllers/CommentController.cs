﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MapAPI.Controllers
{
    public class CommentController : ApiController
    {
        // GET api/values
            [HttpGet]
        [ActionName("getwall")]
        public IEnumerable<Models.Comment> GetWallComment(String username)
        {
            DAO.CommentDao commentDao = new DAO.CommentDao();
            return commentDao.getWallComment(username);
        }

        //get a comment 
        // GET api/Comment/5
            [ActionName("getcomment")]
            public Models.Comment Get(int username)
        {
            DAO.CommentDao commentDao = new DAO.CommentDao();
            Models.Comment comment = commentDao.getComment(username);
            return comment;
        }
        //post a comment to a friend
        // POST api/Comment
        public void Post([FromBody]Models.Comment value)
        {
            DAO.CommentDao commentDao = new DAO.CommentDao();
            commentDao.Add(value);
        }

     
    }
}
