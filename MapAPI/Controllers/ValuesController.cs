﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MapAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<HocSinh> Get()
        {
            List<HocSinh> ls = new List<HocSinh>();
            ls.Add(new HocSinh{ten ="Tran van dan", dtl=10});
            ls.Add(new HocSinh{ten ="Tran van dan", dtl=10});
            return ls;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}